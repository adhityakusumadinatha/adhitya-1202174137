<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'ProfileController@index');
Route::get('/profile/edit/{id}', 'ProfileController@edit');
Route::put('/profile/update/{id}', 'ProfileController@update');
Route::get('/newpost', 'NewPostController@index');
Route::get('/home/detail/{id}', 'HomeController@detail');
Route::post('/newpost/upload', 'NewPostController@upload');
Route::post('/home/komentar', 'HomeController@koment');
Route::post('/home/suka', 'HomeController@suka');
