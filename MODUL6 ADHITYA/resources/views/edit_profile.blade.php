@extends('layouts.app')

@section('content')
<div class="container">
    <div class="form-group">
        <form action="/profile/update/{{ Auth::user()->id }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <h1>Edit Profile</h1>
            <label>Title</label>
            <input type="text" name="title" class="form-control" placeholder="Isi Judul disini.." value=" {{ Auth::user()->title }}">
            <br>
            <label>Description</label>
            <input type="text" name="description" class="form-control" placeholder="Isi Deskripsi disini.." value=" {{ Auth::user()->description }}">
            <br>
            <label>URL</label>
            <input type="text" name="url" class="form-control" placeholder="Isi URL disini.." value="{{ Auth::user()->url }}">
            <br>
            <label>Profile Image</label>
            <br>
            <input type="file" name="avatar">
            <br>
            <br>
            <input type="submit" class="btn btn-primary" value="Save">
        </form>
    </div>
</div>
@endsection