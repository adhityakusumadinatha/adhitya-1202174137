@extends('layouts.app')

@section('content')
<div class="container">
    @if(count($errors) > 0)
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        {{ $error }} <br />
        @endforeach
    </div>
    @endif
    <div class="form-group">
        <form action="/newpost/upload" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="text" name="userID" value="{{ Auth::user()->id }}" hidden="true">
            <h1>Add New Post</h1>
            <label>Post Caption</label>
            <input type="text" name="caption" class="form-control">
            <br>
            <label>Post Image</label>
            <br>
            <input type="file" name="image">
            <br>
            <br>
            <input type="submit" class="btn btn-primary" value="Upload">
        </form>
    </div>
</div>
@endsection