@extends('layouts.app')

@section('content')
<br><br><br>
<div class="container">

    <div class="row">
        <div class="col">
            <img src="{{ url('/uploaded_file/'.$posts->image) }}" alt="" style="width: 500px">
        </div>
        <div class="col">
            <img src="{{ asset('/avatar_file/'.$posts->users->avatar) }}" alt="Avatar" style="width: 50px;"> &nbsp; {{ $posts->users->name}}

            <hr>
            @foreach($posts->komentar_posts as $k)
            <div class="row">
                <b>{{ $k->users->name }}</b>&nbsp; {{ $k->comment}}
            </div>
            @endforeach
            <br>
            <form action="/home/komentar" method="post">
                @csrf
                <div class="form-group form-inline">

                    <input class="form-control" style="width: 92%;" type="text" name="comment" placeholder="Masukan Komentar..">
                    <button type="submit" value="{{ $posts->id }}" class="btn btn-outline-primary" name="tombol_koment" placeholder="Post">Post</button>


                </div>
            </form>
        </div>
    </div>


</div>
@endsection