@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @foreach($posts->reverse() as $p)

            <div class="card">
                <div class="card-header">
                    <!-- avatar -->
                    <!-- <img src="https://lh3.googleusercontent.com/SN5YPRmPRASjOqQmu3-zLZXDi1QToAkVBv5HmS4e1FMvdt3r98O_GsFR5v43trJZngYWPZncWBLRkIMUkvPkQvE1KSEdMG1CsdFaJT-X1O5itT2jLn_udpx10vcVhLqipGB6UsC8dWI=s888-no" alt="" style="width : 50px;" class="rounded-circle"> &nbsp;{{ Auth::user()->name }}</div> -->
                    <img src="{{ url('/avatar_file/'. $p->users->avatar) }}" alt="" style="width : 50px;" class="rounded-circle"> &nbsp;{{ $p->users->name }}</div>

                <div class="card-body">
                    <!-- <a href="/home/detail"><img src="sample_post.jpg" alt="Logo EAD" style="width: 100%;"></a> -->
                    <a class="text-center" href="/home/detail/{{ $p->id }}"><img class="img-fluid" style="width: auto;" src="{{ url('/uploaded_file/'.$p->image) }}" alt="asd"></a>
                </div>
                <div class="card-footer">
                    <div class="emoticon">
                        <form action="/home/suka" method="post" style="display:inline">
                            @csrf
                            <button type="submit" name="tombol_suka" class="btn" value="{{ $p->id }}"><i class="far fa-heart"></i></button>
                        </form>
                        <button type="button" name="button" class="btn"><i class="fa fa-comment"></i></button>
                    </div>
                    <div class="detail">
                        <p>{{ $p->likes }} Likes</p>
                        <p>
                            <b>{{ $p->users->email }}</b> {{ $p->caption }}
                        </p>
                        <hr>
                        @foreach($p->komentar_posts as $komentar)
                        <p>
                            <b>{{ $komentar->users->email }}</b> {{ $komentar->comment }}
                        </p>
                        @endforeach
                    </div>

                    <br>
                    <form action="/home/komentar" method="post">
                        @csrf
                        <div class="form-group form-inline">

                            <input class="form-control" style="width: 92%;" type="text" name="comment" placeholder="Masukan Komentar..">
                            <button type="submit" value="{{ $p->id }}" class="btn btn-outline-primary" name="tombol_koment" placeholder="Post">Post</button>


                        </div>
                    </form>
                </div>

            </div>
            <br>
            <br>

            @endforeach


            <!-- <div class="card">
                <div class="card-header">
                    <img src="https://lh3.googleusercontent.com/SN5YPRmPRASjOqQmu3-zLZXDi1QToAkVBv5HmS4e1FMvdt3r98O_GsFR5v43trJZngYWPZncWBLRkIMUkvPkQvE1KSEdMG1CsdFaJT-X1O5itT2jLn_udpx10vcVhLqipGB6UsC8dWI=s888-no" alt="" style="width : 50px;" class="rounded-circle"> &nbsp;{{ Auth::user()->name }}</div>

                <div class="card-body">
                    <img src="https://lh3.googleusercontent.com/fC12xa7UX7BsjowyJoiosc1O3m8HS9PBaz12f8VSVF_uLiP9BtXaFa63r-mXjFqs1ZFMEi6pOPukcBW24SjQvcOK6b9Rcg7QQdpS7VnDQdOftz-SADnsO8x3kl9xBI-83gHkmsMFTQw=w1583-h890-no" alt="Logo EAD" style="width: 500px;">
                </div>
                <div class="card-footer">
                <b>{{ Auth::user()->email }}</b>
                <br>
                Foto bersama
                </div>
            </div> -->
        </div>
    </div>
</div>
@endsection