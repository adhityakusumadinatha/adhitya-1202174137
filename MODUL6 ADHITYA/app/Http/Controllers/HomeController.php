<?php

namespace App\Http\Controllers;

use App\komentar_posts;
use App\posts;
use App\User;
use Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $home = posts::with('users')->with('komentar_posts')->get();

        return view('home', ['posts' => $home]);
    }

    public function detail($id)
    {
        // $user = User::all();
        // $posts = posts::find($id);
        // $komentar = komentar_posts::all();
        // return view('detail_post',['user'=> $user,'posts' => $posts, 'komentar_posts' => $komentar]);
        $home = posts::with('users')->with('komentar_posts')->where('id', $id)->get();

        return view('detail_post', ['posts' => $home[0]]);
    }

    public function koment(Request $request)
    {
        $komentar = $request->comment;
        $user_id = Auth::user()->id;
        $post_id = $request->tombol_koment;

        komentar_posts::insert([
            'user_id' => $user_id,
            'post_id' => $post_id,
            'comment' => $komentar,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        return redirect('/home');
    }

    public function suka(Request $request)
    {
        $id = $request->tombol_suka;

        $likes = posts::find($id);
        $likes->increment('likes');
        $likes->save();

        return redirect('/home');
    }
}
