<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\posts;
use App\komentar_posts;
use Auth;
class ProfileController extends Controller
{
    public function index(){
        $id = Auth::user()->id;
        $profile = User::with('posts')->find($id);
        $posts = posts::with('users')->find($id);
        
        return view('home_profile',['profile' => $profile, 'posts'=>$posts]);
    }
    public function edit($id){
        $user = User::find($id);
        return view('edit_profile', ['user'=>$user]);
    }

    public function update($id, Request $request){
        $user = User::find($id);
        $user->title = $request->title;
        $user->description = $request->description;
        $user->url = $request->url;
        $avatar = $request->file('avatar');
        $nama_avatar = time()."_".$avatar->getClientOriginalName();
        $tujuan_avatar = 'avatar_file';
        $avatar->move($tujuan_avatar,$nama_avatar);
        $user->avatar =$nama_avatar;
        $user->save();
        
        return redirect('/profile');
    }
}
