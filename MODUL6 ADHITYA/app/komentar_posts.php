<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class komentar_posts extends Model
{
    public $table = "komentar_posts";

    public function users(){
        return $this->belongsTo('App\User','user_id','id');
    }
    public function posts(){
        return $this->belongsTo('App\posts','post_id','id');
    }
}
