<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    protected $table = "posts";
    protected $fillable = ['user_id','caption', 'image', 'likes'];
    public function komentar_posts(){
        return $this->hasMany('App\komentar_posts', 'post_id', 'id');
    }
    public function users(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
