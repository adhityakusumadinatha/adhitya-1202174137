<?php
$namaDriver = $_GET['namaDriver'];
$noTelp = $_GET['noTelp'];
$tglPesan = $_GET['tglPesan'];
$asalDriver = $_GET['driverRadio'];
if (isset($_GET['bawaKresek'])) {
    $bawaKresek = $_GET['bawaKresek'];
} else {
    $bawaKresek = "Tidak";
}
?>

<html>

<head>
    
    <title>Menu</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <style>
        td {
            padding: 20px;
        }
    </style>
    
</head>

<body>
    <div class="container" style="margin-top: 2%;">
        <div class="float-left">
            <div class="text-center">
                <h1 class="display-4">~Data Driver Ojol~</h1>
                <br>
                <p><b>Nama</b></p>
                <p><?php echo $namaDriver; ?></p>
                <p><b>Nomer Telepon</b></p>
                <p>
                    <?php
                    echo $noTelp;
                    ?>
                </p>
                <p><b>Tanggal</b></p>
                <p>
                    <?php
                    echo $tglPesan;
                    ?>
                </p>
                <p><b>Asal Driver</b></p>
                <p>
                    <?php
                    echo $asalDriver;
                    ?>
                </p>
                <p><b>Bawa Kantong</b></p>
                <p>
                    <?php
                    echo $bawaKresek;
                    ?>
                </p>
            </div>
            <div class="text-center">
                <button onclick="window.history.back()" type="button" class="btn btn-primary">
                    << Kembali</button> </div> </div> <div class="float-right">
                        <div class="text-center">
                            <h1 class="display-4">~Menu~</h1>
                            <br>
                            <p class="font-weight-light">Pilih Menu</p>
                            <form action="nota.php" method="POST" >
                                <table>
                                    <tr>
                                        <td>
                                            <div class="from-check">
                                                <input type="checkbox" class="form-check-input" id="esCoklatSusu">
                                                <label for="menu" class="form-check-label">Es Coklat Susu</label>
                                            </div>
                                        </td>
                                        <td>
                                            <label for="menu" class="form-check-label">Rp. 28.000</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="from-check">
                                                <input type="checkbox" class="form-check-input" id="esSusuMatcha">
                                                <label for="menu" class="form-check-label">Es Susu Matcha</label>
                                            </div>
                                        </td>
                                        <td>
                                            <label for="menu" class="form-check-label">Rp. 18.000</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="from-check">
                                                <input type="checkbox" class="form-check-input" id="esSusuMojicha">
                                                <label for="menu" class="form-check-label">Es Susu Mojicha</label>
                                            </div>
                                        </td>
                                        <td>
                                            <label for="menu" class="form-check-label">Rp. 15.000</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="from-check">
                                                <input type="checkbox" class="form-check-input" id="esMatchaLatte">
                                                <label for="menu" class="form-check-label">Es Matcha Latte</label>
                                            </div>
                                        </td>
                                        <td>
                                            <label for="menu" class="form-check-label">Rp. 30.000</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="from-check">
                                                <input type="checkbox" class="form-check-input" id="esTaroSusu">
                                                <label for="menu" class="form-check-label">Es Taro Susu</label>
                                            </div>
                                        </td>
                                        <td>
                                            <label for="menu" class="form-check-label">Rp. 21.000</label>
                                        </td>
                                    </tr>
                                </table>
                                <hr>
                                <div class="container">
                                    <div class="form-group row">
                                        <label for="noOrder" class="col-5 col-form-label">
                                            <p class="font-weight-bold">Nomor Order</p>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="number" class="form-control" id="noOrder" name="noOrder" required>
                                            <div class="invalid-feedback">Nomor Order tidak boleh kosong!</div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="namaPemesan" class="col-5 col-form-label">
                                            <p class="font-weight-bold">Nama Pemesan</p>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="namaPemesan" name="namaPemesan" required>
                                            <div class="invalid-feedback">Nama Pemesan tidak boleh kosong!</div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="email" class="col-5 col-form-label">
                                            <p class="font-weight-bold">Email</p>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="Email" class="form-control" id="emailPesanan" name="emailPesanan" required>
                                            <div class="invalid-feedback">Email tidak boleh kosong!</div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="alamatOrder" class="col-5 col-form-label">
                                            <p class="font-weight-bold">Alamat Order</p>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="alamatOrder" name="alamatOrder" required>
                                            <div class="invalid-feedback">Alamat Order tidak boleh kosong!</div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="member" class="col-5 col-form-label">
                                            <p class="font-weight-bold">Member</p>
                                        </label>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="member" id="memberYes" value="Ya" required>
                                            <label class="form-check-label" for="option1">Ya</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="member" id="memberNo" value="Tidak" required>
                                            <label class="form-check-label" for="option2">Tidak</label>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group row">
                                        <label for="metodePembayaran" class="col-5 col-form-label">
                                            <p class="font-weight-bold">Metode Pembayaran</p>
                                        </label>
                                        <div class="col-sm-7">
                                            <select name="metodePembayaran" id="pilihPembayaran" class="custom-select">
                                                <option selected>--Pilih Metode Pembayaran--</option>
                                                <option value="Cash">Cash</option>
                                                <option value="E-Money(OVO/GO-PAY)">OVO / GO-PAY</option>
                                                <option value="Credit Card">Credit Card</option>
                                                <option value="Lainnya">Lainnya</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <input type="text" class="hasil" id="hasil" name="hasil" value="" style="visibility: hidden">
                                        <input type="submit" value="SUBMIT" class="btn btn-primary" onclick="done()">
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
            </div>


        </div>
</body>
<script>
        function done(){
            var total = 0;
            if($('#esCoklatSusu').is(':checked')) {
               total=28000+total;
           }
           if($('#esSusuMatcha').is(':checked')) {
               total=18000+total;
           }
           if($('#esSusuMojicha').is(':checked')) {
               total=15000+total;
           }
           if($('#esMatchaLatte').is(':checked')) {
               total=30000+total;
           }
           if($('#esTaroSusu').is(':checked')) {
               total=21000+total;
           }
           document.getElementById("hasil").value = total;
           var yakin = confirm("Apakah Anda Yakin?");
           if (yakin == false) {
               event.preventDefault();
           }
        }
    </script>
</html>