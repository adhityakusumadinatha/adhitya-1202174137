<?php
$hasil = $_POST['hasil'];
$idPesanan = $_POST['noOrder'];
$namaPemesan = $_POST['namaPemesan'];
$emailPesanan = $_POST['emailPesanan'];
$alamat = $_POST['alamatOrder'];
$member = $_POST['member'];
if ($member == "Ya") {
    $hasil = $hasil - ($hasil * (10 / 100));
}
$hasil = number_format($hasil, 2, ",", ".");
$pembayaran = $_POST['metodePembayaran'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=p, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nota</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

</head>

<body>
    <div class="container">
        <div class="text-center">
            <h1 class="display-3">Transaksi</h1>
            <h1 class="display-3">Pemesanan</h1>
            <p class="font-weight-light">Terima kasih telah berbelanja dengan Kopi Susu Duarr!</p>
            <h1 class="display-4">
                <?php
                echo "Rp. $hasil";
                ?>
            </h1>
            <hr style="margin: 30px auto; width:500px;">
            <div class="form-group row" style="margin: 30px auto; width:500px;">   
            <label class="col-4 col-form-label">
                    <p class="font-weight-bold">ID</p>
                </label>
                <div class="col-sm-8">
                    <p class="form-control" style="border: hidden;"><?php echo $idPesanan ?></p>
                </div>
            </div>
            <hr style="margin: 30px auto; width:500px;">
            <div class="form-group row" style="margin: 30px auto; width:500px;">   
            <label class="col-4 col-form-label">
                    <p class="font-weight-bold">Nama</p>
                </label>
                <div class="col-sm-8">
                    <p class="form-control" style="border: hidden;"><?php echo $namaPemesan ?></p>
                </div>
            </div><hr style="margin: 30px auto; width:500px;">
            <div class="form-group row" style="margin: 30px auto; width:500px;">   
            <label class="col-4 col-form-label">
                    <p class="font-weight-bold">Email</p>
                </label>
                <div class="col-sm-8">
                    <p class="form-control" style="border: hidden;"><?php echo $emailPesanan ?></p>
                </div>
            </div><hr style="margin: 30px auto; width:500px;">
            <div class="form-group row" style="margin: 30px auto; width:500px;">   
            <label class="col-4 col-form-label">
                    <p class="font-weight-bold">Alamat</p>
                </label>
                <div class="col-sm-8">
                    <p class="form-control" style="border: hidden;"><?php echo $alamat ?></p>
                </div>
            </div><hr style="margin: 30px auto; width:500px;">
            <div class="form-group row" style="margin: 30px auto; width:500px;">   
            <label class="col-4 col-form-label">
                    <p class="font-weight-bold">Member</p>
                </label>
                <div class="col-sm-8">
                    <p class="form-control" style="border: hidden;"><?php echo $member ?></p>
                </div>
            </div><hr style="margin: 30px auto; width:500px;">
            <div class="form-group row" style="margin: 30px auto; width:500px;">   
            <label class="col-4 col-form-label">
                    <p class="font-weight-bold">Pembayaran</p>
                </label>
                <div class="col-sm-8">
                    <p class="form-control" style="border: hidden;"><?php echo $pembayaran ?></p>
                </div>
            </div>

        </div>

    </div>

</body>

</html>