<?php

session_start();
include('config.php');
$query = "SELECT * FROM cart_table";
$select = mysqli_query($conn, $query);
$nama;
if (isset($_SESSION['username'])) {
    # code...


    ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Cart</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <style>
            table {
                counter-reset: tableCount;
            }

            .counterCell:before {
                content: counter(tableCount);
                counter-increment: tableCount;
            }
        </style>




    </head>

    <body>
        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-light bg-light">
            <a class="navbar-brand" href="home2.php">
                <img src="../Modul1/EAD.png" alt="Logo EAD" style="width: 150px;"></a>
            <div class="d-flex flex-row-reverse bd-highlight">

                <div class="nav-item dropdown" style="padding: 10px;">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= $_SESSION['username'] ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="editprofile.php">Edit Profile</a>
                        <a class="dropdown-item" href="logout.php">Logout</a>
                    </div>

                </div>
                <a class="navbar-text" href="cart.php" style="padding: 20px;">
                    <ion-icon name="cart"></ion-icon>
                </a>
            </div>
        </nav>
        <!-- Navbar -->
        <br><br>
        <br>
        <br>
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <table style="text-align: left" class="table table-striped table-inverse table-responsive">
                    <thead class="thead-inverse">
                        <tr>
                            <th>No</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $userID = $_SESSION['id'];
                            $sql = mysqli_query($conn, "SELECT * FROM cart_table WHERE user_id = '$userID'");
                            $total = 0;
                            while ($selects = mysqli_fetch_assoc($sql)) { ?>
                            <tr>
                                <td class="counterCell"></td>
                                <td><?= $selects['product'] ?></td>
                                <td>Rp. <?= $selects['price'] ?>.000</td>
                                <td>
                                    <a name="delete" id="delete" class="btn btn-danger" href="delete.php?idCart=<?= $selects['id'] ?>" role="button">DELETE</a>
                                </td>
                                <?php $total += $selects['price'];?>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="2" style="text-align: center;">Total</td>
                            <td>Rp.<?php echo $total;?>.000</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.18.6/dist/sweetalert2.all.min.js"></script>
        <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>

    </html>

<?php
} else {
    echo "        
                <script type='text/javascript'>
                swal.fire({type: 'error', title: 'Oops!',text: 'Anda belum login!'}).then(function(){
                window.location = 'home.php';
                    }
                );
            </script>";
}
?>