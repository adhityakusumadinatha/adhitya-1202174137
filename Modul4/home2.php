    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Home</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    </head>
    <body>
    <?php
    
    session_start();

    if (isset($_SESSION['username'])) {
        # code...


        ?>


            <!-- Navbar -->
            <nav class="navbar fixed-top navbar-light bg-light">
                <a class="navbar-brand" href="home2.php">
                    <img src="../Modul1/EAD.png" alt="Logo EAD" style="width: 150px;"></a>
                <div class="d-flex flex-row-reverse bd-highlight">

                    <div class="nav-item dropdown" style="padding: 10px;">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= $_SESSION['username'] ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="editprofile.php">Edit Profile</a>
                            <a class="dropdown-item" href="logout.php">Logout</a>
                        </div>

                    </div>
                    <a class="navbar-text" href="cart.php" style="padding: 20px;">
                        <ion-icon name="cart"></ion-icon>
                    </a>
                </div>
            </nav>
            <!-- Navbar -->
            <br><br>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="card bg-dark text-white">
                            <img src="https://img.freepik.com/free-vector/gradient-geometric-shape-background_78532-374.jpg?size=626&ext=jpg" class="card-img" alt="Gambar" style="display: block; height: 180px;">
                            <div class="card-img-overlay">
                                <h1 class="card-title">Hello Coders!</h1>
                                <p class="card-text">Welcome to our store, please take a look for the products you might buy.</p>
                            </div>
                        </div>

                    </div>


                </div>
                <br>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <!-- <img class="card-img-top" src="https://this.deakin.edu.au/wp-content/uploads/2016/05/Pink-desk-with-computer.jpg" alt="Card image"> -->
                            <ion-icon name="globe" style="width: 100%; font-size: 250px; color: #a55eea;"></ion-icon>
                            <div class="card-body">
                                <h4 class="card-title">Learning Basic Web Programming</h4>
                                <p class="card-text"><b>Rp. 210.000,-</b></p>
                                <p class="card-text">Want to be able to make a website? Learn basic components such as HTML, CSS and JavaScript in this class curriculum.</p>
                                <br>
                                <a name="but" href="buy.php?product=Learning+Basic+Web+Programming&price=210" class="btn btn-primary" style="width: 100%;">Buy</a>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- <img class="card-img-top" src="https://this.deakin.edu.au/wp-content/uploads/2016/05/Pink-desk-with-computer.jpg" alt="Card image"> -->
                            <ion-icon name="logo-javascript" style="width: 100%; font-size: 250px; color: #4834d4;"></ion-icon>
                            <div class="card-body">
                                <h4 class="card-title">Starting Programming in JavaScript</h4>
                                <p class="card-text"><b>Rp. 150.000,-</b></p>
                                <p class="card-text">Learn JavaScript Language for you who want to learn the most popular Object-Oriented Programming concepts for developing applications.</p>
                                <a href="buy.php?product=Starting+Programming+in+Javascript&price=150" class="btn btn-primary" style="width: 100%;">Buy</a>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <!-- <img class="card-img-top" src="https://this.deakin.edu.au/wp-content/uploads/2016/05/Pink-desk-with-computer.jpg" alt="Card image"> -->
                            <ion-icon name="logo-python" style="width: 100%; font-size: 250px; color: #1e90ff"></ion-icon>
                            <div class="card-body">
                                <h4 class="card-title">Starting Programming in Python</h4>
                                <p class="card-text"><b>Rp. 200.000,-</b></p>
                                <p class="card-text">Learn Python - Fundamental various current industry trens Data Science, Machine Learning, Infrastructure Management.</p>
                                <a href="buy.php?product=Starting+Programming+in+Python&price=200" class="btn btn-primary" style="width: 100%;">Buy</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    © EAD STORE
                </div>
                <!-- Modal Login -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">Login</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <!-- Form Login-->
                                <form action="home.php" method="post">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" value="Login" name="submit" class="btn btn-primary">
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- Modal Register -->
                <div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalCenterTitle">Register</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <!-- Form Register-->
                                <form action="logout.php" method="post">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Username</label>
                                        <input name="username" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter username">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Confirm Password</label>
                                        <input name="repassword" type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="window.location = 'logout.php'">Close</button>
                                        <input type="submit" value="Register" name="submit" class="btn btn-primary">
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            <?php
            } else {
                echo "        
                <script type='text/javascript'>
                swal.fire({type: 'error', title: 'Oops!',text: 'Anda belum login!'}).then(function(){
                window.location = 'home.php';
                    }
                );
            </script>";
            }
            ?>
            <!-- Script CSS -->
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.18.6/dist/sweetalert2.all.min.js"></script>
            <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        </body>


    </html>